/*function sampleFunction(){
	alert(`Hello, I am a Filipino.`)
};

sampleFunction();

const sampleFunction1 = () => {
	alert('I am an arrow function');
};

sampleFunction1();
*/
/*implicit return with arrow function*/

function prodNum(num1, num2){
	return	num1 * num2
};

let prod = prodNum(25, 25)
console.log(prod);

/*solution*/

const prodNum1 = (num1, num2) => num1 * num2;

let product = prodNum1(25,10);
console.log(product);

const multiplyNum = (num1, num2) => {  /*if you want to insert a return or use curly braces {}, or if we want to have or insert block of codes.*/
	return num1*num2
};

let prod2 = multiplyNum(5,10);
console.log(prod2);

/*convert traditional function to arrow function with if else*/


/*function ans(a){
	if(a < 10){
		return 'true';
	} else {
		return 'false';
	}
};

let answer = ans(5);
console.log(answer);
*/
/*ternary operator ?: 


syntax: condition ? expression1 : expression2*/
/*const ans = (a) => (a < 10) ? 'true' : 'false';
let answer = ans(5);
console.log(answer);

let mark = prompt('Enter your grade: ');

let grade = (mark >= 75) ? 'Passed' : 'Failed';
console.log(`You ${grade} the exam`);
*/
/*mini activity*/

let numbers = [1,2,3,4,5]

const allValid = numbers.every(number => 
console.log(number < 3));
console.log(allValid);

const filterValid = numbers.filter(number => number < 3);
console.log(filterValid);

const numberMap = numbers.map(number => number * number);
console.log(numberMap);

/*to check if number is positive, negative or zero using ternary operators*/
let num = 3;
let result = (num >= 0) ? (num == 0 ? 'zero' : 'positive') : 'negative';

console.log(`The number is ${result}`);


/*JSON - JavaScript Object Notation
	- is a string while JS Object is an object.
	- is a language for data transfer
	- popularly used to pass data from one application to another
	- not only in JS but also in other programming languages (passing of data)
	- .json
	- JSON keys are surrounded with double quotes "".
		syntax: 
			{
				"key1" : "value1",
				"key2" : "value2"
			}
	*/

	const person = {
		"name" : "Juan",
		"weight" : 175,
		"age" : 20,
		"eyeColor" : "brown",
		"cars" : ["Toyota", "Honda"],
		"favoriteBooks" : {
			"title" : "When the fire nation attacked",
			"author" : "nickelodeon",
			"release" : "2021"

		}
	}

	console.log(typeof(person));

	/*mini-activity*/


	const assets = [
	{
		"id" : 1,
		"name" : "ming",
		"description" : "human",
		"isAvailable" : true,
		"dateAdded" : "may 21, 1982"
	},

	{
		"id" : 4,
		"name" : "jung",
		"description" : "alien",
		"isAvailable" : false,
		"dateAdded" : "august 11, 1999"
	}
	];

	console.log(assets)

	/*XML FORMAT - extensible markup language

	<note>
		<to>Juan</to>
		<from>Maria Clara</from>
		<heading>Reminder</heading>
		<body>
			dont forget me this weekend
		</body>
	</note>

	*/

	/*JSON - key value pairs - string*/
	
/*	{
		"to" : "Juan",
		"from" : "Maria Clara",
		"heading" : "Reminder",
		"body" : "dont forget me this weekend!"

	}
*/
	/*stringify - method to convert JavaScript to JSON and vice versa*/

	/*CONVERTING JS OBJECTS TO JSON
		- this is commonly used when trying to pass data from one applicaiton to another via the use of HTTP request
			-HTTP requests are requests for resource between a server and a client (broswer)
		- JSON format is also in database
			*/



	const cat = {
		"name" : "Mashiro",
		"age" : 3,
		"weight" : 20
	}

	console.log(cat);

	const catJSON = JSON.stringify(cat);
	console.log(catJSON);

	/*parse - use to reverse stringify '{ }'    */

	const json ='{"name" : "Mashiro", "age" : 3,"weight" : 20}'
	const cat1 = JSON.parse(json)
	console.log(cat1);

	let batchesArr = [
		{
			batchName : "Batch 131"
		},
		{
			batchName : "Batch 132"
		}
	];

	console.log(JSON.stringify(batchesArr));

	let batchesJ = `[
	{
		"batchName":"Batch 131"
	},
	{
		"batchName":"Batch 132"
	}
	]`;

	console.log(JSON.parse(batchesJ));