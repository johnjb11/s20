/*s20 activity*/

let courseArr = [], postCourse = (id, name, description, price, isActive) => {
	let course = {id, name, description, price, isActive}
	courseArr.push(course);
	console.log(course)
	alert(`You have created ${name}. The price is ${price}.`)
	
}

postCourse('A1', 'Accounting', 'Finance', '20 USD', true);
postCourse('P2', 'Psychology', 'Social Science', '10 USD', true);


const findCourse = (id) => {
	let foundCourse = courseArr.find(element => element.id === id) 
	return foundCourse;
}

console.log(findCourse('P2'))

console.log(findCourse('A1'))


const deleteCourse = () => courseArr.pop();
deleteCourse();
console.log(courseArr);
